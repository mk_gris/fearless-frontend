window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const loadingTag = document.getElementById('loading-conference-spinner');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      // Here, add the 'd-none' class to the loading icon
      loadingTag.classList.add('d-none');

      // Here, remove the 'd-none' class from the select tag
      selectTag.classList.remove('d-none');
    }
    const formTag = document.getElementById('create-attendee-form');
    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const attendeeData = Object.fromEntries(formData.entries());
        const url = 'http://localhost:8001/api/attendees/';
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(attendeeData),
        };
        try {
            const response = await fetch(url, options);
            if (response.ok) {
                console.log('Attendee submitted successfully!');
            } else {
                console.error("Failed to submit attendee.");
            }
        } catch (error) {
            console.error('An error occurred while submitting attendee:', error);
        }
    })


  });
