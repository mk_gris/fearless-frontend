function createCard(name, description, pictureUrl, fStartDate, fEndDate, locationName) {
    return `
        <div class="col-4">
        <div class="card shadow p-3 mb-5 bg-white rounded py-1 ">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
            <p class="card-text">${description}</p>
            </div>
        <div class ="card-footer">${fStartDate} - ${fEndDate}</div>
        </div>
        </div>
    `;
  }


  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const locationName = details.conference.location.name;
            const startDate = new Date(details.conference.starts);
            const endDate = new Date(details.conference.ends);
            const options = {month: '2-digit', day: '2-digit', year: 'numeric'};
            const fStartDate = startDate.toLocaleDateString('en-US', options);
            const fEndDate = endDate.toLocaleDateString('en-US', options);
            const html = createCard(title, description, pictureUrl, fStartDate, fEndDate, locationName);
            const column = document.querySelector('.row');
            column.innerHTML += html;

            console.log(details);
          }
        }

      }
    } catch (e) {
        console.error(e);
      // Figure out what to do if an error is raised
    }

  });

// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//         const response = await fetch(url);

//         if (!response.ok) {
//             throw new Error('Response not ok');
//         } else {
//             const data = await response.json();
//         }
//     } catch (error) {
//         console.error('error', error);
//     }

// });
